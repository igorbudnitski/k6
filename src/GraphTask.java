import java.util.*;
import java.util.stream.IntStream;
import static java.lang.String.format;

/**
 * Task description:
 * Koostada meetod, mis arvutab etteantud sidusa lihtgraafi G=(V, E) iga tipu v jaoks välja selle ekstsentrilisuse:
 * e(v) = max {d(u, v)| u kuulub hulka V } , kus d(u, v) on tipu u kaugus tipust v (lühima tee pikkus tipust u tippu v)
 */


/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomTree (4);
      long startTime = System.currentTimeMillis();
      // checking the time to run the method
      int [] eccentricities = eccentricitiesSearch(g);
      // printing the result, min and max results
      System.out.println(format("%d ms", System.currentTimeMillis() - startTime));
      System.out.println(format("result eccentricities count: %d%nmin: %d, max: %d",
              eccentricities.length, Arrays.stream(eccentricities).min().getAsInt(), Arrays.stream(eccentricities).max().getAsInt()));


      // tree
/*      g = new Graph ("G1");
      g.createRandomTree (2);
      eccentricities = eccentricitiesSearch(g);
      System.out.println(Arrays.toString(eccentricities)); // prints [1, 1]*/

      // simple graphg = new Graph ("G2");

      g.createRandomSimpleGraph (3, 2);
      System.out.println("Input: " + g);
      eccentricities = eccentricitiesSearch(g);
      System.out.println("Result eccentricities for all vertices: " + Arrays.toString(eccentricities));
      System.out.println(format("result eccentricities count: %d%nmin: %d, max: %d",
              eccentricities.length, Arrays.stream(eccentricities).min().getAsInt(), Arrays.stream(eccentricities).max().getAsInt()));

/*      Graph g1 = new Graph ("G1");
      g1.createRandomTree (5);
      System.out.println(g1);
      eccentricities = eccentricitiesSearch(g1);
      System.out.println(Arrays.toString(eccentricities));
      IntStream.rangeClosed(1, 5).map(i -> findEccentricityForVertex(g1, i)).forEach(System.out::println);*/

   }

   /**
    * Method description: finds eccentricities for the graph
    * @param g graph
    * @param vertexNumber vertex number
    * @return array of eccentricities, in order corresponding to vertex numbering
    */
/*   private int findEccentricityForVertex(Graph g, int vertexNumber) {
      return eccentricitiesSearch(g)[vertexNumber - 1];
   }*/

   /**
    * This method searches for the eccentricities
    * @param g graph
    * @return eccentricities
    */

   private int [] eccentricitiesSearch (Graph g){
      //võtan külgmaatriksi (teie funktsioon)
      int[][] adjMatrix = g.createAdjMatrix();
      printMatrix(adjMatrix, "Adjacency matrix:");
      int countV = adjMatrix.length;
      //lugeb vahemaatriksit
      int[][] distMatrix = distMatrixSearch(adjMatrix);
      printMatrix(distMatrix, "Distance matrix:");
      //teen massiv kalulatsiooni jargi
      int[] eccentricities = new int[countV];
      for (int i = 0; i < countV; i++){
         // võtke iga tipu jaoks maksimaalne kaugus mis tahes teise punkti, see on selle ekstsentrilisus
         eccentricities[i] = Arrays.stream(distMatrix[i]).max().getAsInt();
      }
      return eccentricities;
   }

   /**
    * converting the adjacency matrix to distance matrix using Floyd-Warshall algorithm
    * @param adjMatrix adj matrix
    * @return Distance Matrix
    */

   private int[][] distMatrixSearch(int[] [] adjMatrix){
      int countV = adjMatrix.length;
      // pean kõigepealt maatriksi kopeerima ja valmistama
      int[][] distMatrix = matrixPrepare(adjMatrix, countV);
      // seejärel kolm algoritmi sisestatud silmust, võrdleb vahemaid ja asendab need uutega
      for (int k = 0; k < countV; k++){
         for (int i = 0; i <countV; i++){
            for (int j = 0; j < countV; j++){
               if ( i == j){
                  // kui need indeksid langevad kokku, siis on see sama tipp, siis seadsime 0 kauguse enda juurde
                  distMatrix[i][j] = 0;
               }else
                  // vaatan väikseimat olemasoleva vahemaa ja uue tee vahel
                  distMatrix[i][j] = Math.min(distMatrix[i][j], distMatrix[i][k] + distMatrix[k][j]);
            }
      }
      }
      return distMatrix;
   }

   /**
    * prepare matrix for calculations
    * @param matrix matrix
    * @param size size
    * @return copied matrix but with all zeros changed to very high integer
    */
   private int [][] matrixPrepare(int[][] matrix, int size){
      int[][] m = new int[size][size];
      for (int i = 0; i < size; i++){
         for (int j = 0; j < size;j++){
            m[i][j] = matrix[i][j] > 0 ? matrix[i][j] : 9999999;
         }
      }return m;
   }

   /**
    * Prints matrix
    * @param m matrix
    * @param s string
    */

   private void printMatrix(int[][] m, String s){
      System.out.println("\n" + s);
      for (int i = 0; i < m.length; i++){
         System.out.println(Arrays.toString(m[i]));
      }
   }

   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         System.out.println("Number of vertices in the tree: " + n);
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         System.out.println("Number of vertices: " + n + " Number of edges: " + m);
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }
   }

} 

